package repo

import (
	"fmt"

	"github.com/jinzhu/gorm"
	c "github.com/nbriar/authenz-go/configuration"

	// Adapter for gorm
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/nbriar/authenz-go/migrations"
)

// DBRepo is the primary datastore
var DBRepo *gorm.DB

func init() {
	var err error
	psqlInfo := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
		c.DB.Host, c.DB.User, c.DB.DBPassword, c.DB.DBName)

	DBRepo, err = gorm.Open(c.DB.Adapter, psqlInfo)

	migrations.Run(DBRepo)

	if err != nil {
		fmt.Println(err)
		panic("failed to connect database")
	}
}
