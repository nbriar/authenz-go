package migrations


	import (
		"fmt"
    "github.com/golang-migrate/migrate/v4"
    _ "github.com/golang-migrate/migrate/v4/database/postgres"
    _ "github.com/golang-migrate/migrate/v4/source/github"
		"github.com/nbriar/authenz-go/user"
)

// Run migrations
func Run(db *gorm.DB) {
	fmt.Println("Performing migrations...")
	// TODO fix this because it's not working
	db.AutoMigrate(&user.User{})
	fmt.Println("Migrations complete.")
}
